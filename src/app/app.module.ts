import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ErrorComponent } from './component/error/error.component';
import { SplashComponent } from './page/splash/splash.component';
import { RegistreComponent } from './page/registre/registre.component';
import { Form1Component } from './component/form1/form1.component';
import { QuestionComponent } from './page/question/question.component';
import { KeyframeComponent } from './page/keyframe/keyframe.component';
import { FinishComponent } from './page/finish/finish.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    SplashComponent,
    RegistreComponent,
    Form1Component,
    QuestionComponent,
    KeyframeComponent,
    FinishComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
