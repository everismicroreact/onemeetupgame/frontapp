import { Injectable } from '@angular/core';
import { IUser } from 'src/app/interfaces/i.user';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {


  public user: IUser;

  constructor() { }

  /**
   * Funcion encarga de enviar el usuario actual
   * @param user Usuario que ingresa a la plataforma
   */
  public insertUser(user: IUser) {
    this.user = user;
  }

  /**
   * Funcion que retorna la informacion del usuario actual
   */
  public getUser(): IUser {
    return this.user;
  }
}
