import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor() {
  }

  public createEventSource(path: string): EventSource {
    return new EventSource(path);
  }
}
