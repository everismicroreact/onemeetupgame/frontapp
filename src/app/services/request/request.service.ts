import { Injectable } from '@angular/core';
import { HTTP_METHODS, END_POINTS, BASE_URL } from 'src/app/enums/e.request';
import { HttpClient, HttpRequest, HttpEvent, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class RequestService {


  constructor(
    private http: HttpClient,

  ) {
  }

  /**
   * Funcion encargada de realizar el metodo http
   * @param method Metodo que se va a ejecutar GET, POST, PUT, DELETE
   * @param url URL base para ejecutar la peticion
   * @param endPoint Endpoint especifco para realizar la peticion
   * @param body Cuerpo del mensaje
   * @param query Query que va en la url
   */
  // tslint:disable-next-line: max-line-length
  public makeRequest(method: HTTP_METHODS, url: BASE_URL, endPoint: END_POINTS, body?: any, query: string = ''): Promise<HttpEvent<unknown>> {
    const finalUrl = `${url}${endPoint}${query}`;
    const headers = this.createHeaderes();
    const request = new HttpRequest(
      method,
      finalUrl,
      JSON.stringify(body),
      {
        headers
      }
    );

    console.log('url', finalUrl);
    console.log('headers', headers);
    console.log('method', method);
    console.log('body', body);
    return this.http.request(request).toPromise();
  }

  /**
   * Funcion encargada de crear los headers
   */
  private createHeaderes(): HttpHeaders {
    const header = {
      'Content-Type': 'application/json',
    };
    return new HttpHeaders(header);
  }
}
