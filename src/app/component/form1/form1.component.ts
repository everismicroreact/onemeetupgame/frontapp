import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IUser } from 'src/app/interfaces/i.user';
import { RequestService } from 'src/app/services/request/request.service';
import { CREATE_REQUEST_USER } from 'src/app/models/m.user';
import { HTTP_METHODS, END_POINTS, BASE_URL } from 'src/app/enums/e.request';
import { Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global/global.service';


@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit {
  public registreForm: FormGroup;


  constructor(
    private request: RequestService,
    private router: Router,
    private global: GlobalService,
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  /**
   * Funcion que se encarga de crear un formulario
   */
  private createForm() {
    this.registreForm = new FormGroup({
      name: new FormControl('', [
        Validators.required
      ]),
      dni: new FormControl('', [
        Validators.required,
      ]),
      color: new FormControl('black', [
        Validators.required
      ]),
      numberCar: new FormControl('', [
        Validators.required
      ])
    });
  }


  /**
   * Funcion que se encarga de enviar la informacion del registro al servidor
   * 1. Realiza la peticion
   * 2. Cambia a la vista de question
   */
  public sendInformation() {
    const user: IUser = {
      name: this.registreForm.value.name,
      indentification: this.registreForm.value.dni,
      vehicle: {
        color: this.registreForm.value.color,
        number: this.registreForm.value.numberCar,
      }
    };

    this.global.insertUser(user);
    const body = CREATE_REQUEST_USER(user);
    this.request.makeRequest(HTTP_METHODS.POST, BASE_URL.REGISTRE, END_POINTS.REGISTRE, body).then((response: any) => {

      if (response.status === 201) {
        this.router.navigateByUrl('/question');
      } else {
        console.log('error', response);
      }

    }).catch((err) => {
      console.log(err);
    });
  }



}
