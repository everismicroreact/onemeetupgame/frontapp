/**
 * Propiedades de una pregunta
 */
export interface IQuestion {
  description: string;
  id: string;
  level: number;
  options: IAnswer[];
  time?: number;
}

/**
 * Propiedades de una respuesta
 */
export interface IAnswer {
  description: string;
  letterOption: string;
  correct: boolean;
}
