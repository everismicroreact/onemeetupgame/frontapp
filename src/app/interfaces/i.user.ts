/**
 * Propiedades que menja el usuario
 */
export interface IUser {
  name?: string;
  indentification?: string;
  vehicle?: {
    color: string,
    number: string
  };
  mov?: number;
  position?: {
    track: number,
    line: number,
    step: number
  };
  points?: number;
}
