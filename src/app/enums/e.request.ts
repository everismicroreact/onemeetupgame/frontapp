/**
 * Endpoints
 */
export enum END_POINTS {
  REGISTRE = '/register',
  QUESTION = '/challenge'
}
/**
 * Url bases
 */
export enum BASE_URL {
  REGISTRE = 'https://game.everisdigital.com/api',
  QUESTION = 'https://game.everisdigital.com/api'
}
/**
 * Metodos HTTP
 */
export enum HTTP_METHODS {
  GET = 'GET',
  PUT = 'PUT',
  POST = 'POST',
  DELETE = 'DELETE'
}
