import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { ErrorComponent } from './component/error/error.component';
import { SplashComponent } from './page/splash/splash.component';
import { RegistreComponent } from './page/registre/registre.component';
import { QuestionComponent } from './page/question/question.component';
import { KeyframeComponent } from './page/keyframe/keyframe.component';
import { FinishComponent } from './page/finish/finish.component';


/**
 * Rutas con las que cuenta el aplicativo
 */
const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'registre', component: RegistreComponent },
  { path: 'run', component: KeyframeComponent },
  { path: 'splash', component: SplashComponent },
  { path: 'question', component: QuestionComponent },
  { path: 'error', component: ErrorComponent },
  { path: 'finish', component: FinishComponent },
  { path: '**', component: ErrorComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
