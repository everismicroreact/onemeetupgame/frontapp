import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.css']
})
export class SplashComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  /**
   * Funcion que se encarga de ejecutarse cuando arranca la vista
   * Tiene la responsabilidad de dar una espera de 3 segundo para cambiar al registro
   */
  ngOnInit() {
    setTimeout(() => {
      this.router.navigateByUrl('/registre');
    }, 3000);
  }

}
