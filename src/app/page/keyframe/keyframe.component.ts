import { Component, OnInit, ɵConsole } from '@angular/core';
import { IUser } from 'src/app/interfaces/i.user';
import { EventsService } from 'src/app/services/events/events.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-keyframe',
  templateUrl: './keyframe.component.html',
  styleUrls: ['./keyframe.component.css']
})
export class KeyframeComponent implements OnInit {


  public listUsers: IUser[];
  public listUsersPosition: IUser[];
  public eventRegistre: EventSource;
  public eventRegistreStatus: EventSource;
  public timer: any;
  public finishRun = true;

  movimiento = '0%';
  count = 0;

  constructor(
    private eventService: EventsService
  ) { }

  /**
   * Funcion que se ejecuta cunando inicializa la vista, se encarga de inicar los SSE
   */
  ngOnInit() {
    this.listUsers = [];
    this.listUsersPosition = [];
    // this.mockUser();
    this.managerEventRegistre();
  }

  /**
   * Funcion que devuelve un mock de un usuario
   * @param user usuario al cual se le quiere completar los datos de manera aleatoria
   */
  private getMockUser(user: IUser): IUser {
    const gamer = Object.assign({}, user);
    gamer.name = 'Corredor: ' + Math.floor(Math.random() * 50);
    gamer.vehicle = {
      color: '#' + Math.random().toString(16).slice(-6),
      number: `${Math.floor(Math.random() * 50)}`
    };
    return gamer;
  }

  /**
   * Funcion encargada de inicialziar los eventos SSE correspondientes a la posicion, y al estado de la posicion
   */
  private managerEventRegistre() {

    this.eventRegistre = this.eventService.createEventSource('https://game.everisdigital.com/api/position');
    this.eventRegistre.addEventListener('message', message => {
      this.managerPosition(message);
    });
    this.eventRegistre.onerror = error => {
      console.log(error);
    };

    this.eventRegistreStatus = this.eventService.createEventSource('https://game.everisdigital.com/api/posstatus');
    this.eventRegistreStatus.addEventListener('message', message => {
      this.finishRun = JSON.parse(message.data);
      console.log(this.finishRun);
      if (!this.finishRun) {

      }
    });
  }

  private managerPosition(data: any) {
    const player = JSON.parse(data.data);
    const user: IUser = player;
    user.name = player.player.user;
    user.indentification = player.player.identification;
    const exist = this.validateExistUser(user);
    if (!exist) {
      if (!user.vehicle.color) {
        user.vehicle.color = '#' + Math.random().toString(16).slice(-6);
      }
      user.mov = 0;
      this.listUsers.push(user);
    } else {
      const userExist = this.getUserBiID(user.indentification);
      user.position.step = user.position.step > 21 ? 21 : user.position.step;
      userExist.mov = user.position.step * 90 / 20;
      userExist.points = user.points;
    }
    this.filterPosition();
  }


  private getUserBiID(identification: string) {
    const searchUser = this.listUsers.filter((user: IUser) => user.indentification === identification);
    return searchUser[0];
  }

  private validateExistUser(user: IUser) {
    let exist = false;
    if (this.listUsers.length > 0) {
      this.listUsers.forEach((userList: IUser) => {
        if (userList && userList.indentification === user.indentification) {
          exist = true;
        }
      });
    }
    return exist;
  }

  public filterPosition() {
    this.listUsersPosition = this.listUsers.sort((a, b) => b.points > a.points ? 1 : -1);
    return this.listUsersPosition;
  }


}
