import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/services/request/request.service';
import { HTTP_METHODS, BASE_URL, END_POINTS } from 'src/app/enums/e.request';
import { IQuestion, IAnswer } from 'src/app/interfaces/i.question';
import { CONVERT_QUESTION_TO_REQUEST_VALIDATE } from 'src/app/models/m.question';
import { IUser } from 'src/app/interfaces/i.user';
import { GlobalService } from 'src/app/services/global/global.service';
import { EventsService } from 'src/app/services/events/events.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {


  public newQuestion: IQuestion;
  private timer: any;
  public eventRegistre: EventSource;
  public eventRegistreStatus: EventSource;
  private countTime = 0;
  public startQuestion = false;
  public messageload = 'Esperando a los demas competidores';

  constructor(
    private request: RequestService,
    private global: GlobalService,
    private eventService: EventsService,
    private router: Router,
  ) { }

  /**
   * Funcion que se ejecuta cuando se dibuja la vista, se encarga de inicializar los SSE
   */
  ngOnInit() {
    this.startRegistre();
    setTimeout(() => {
      this.statusPosition();
    }, 500);
  }

  /**
   * Funcion encargada de iniciar el SSE correspondiente al estado del servicio
   */
  private startRegistre() {
    this.eventRegistre = this.eventService.createEventSource('https://game.everisdigital.com/api/regstatus');
    this.eventRegistre.addEventListener('message', message => {
      this.startQuestion = JSON.parse(message.data);
      console.log('Register', this.startQuestion);
      if (!this.startQuestion) {
        this.loadQuestion();
      }
    });

    this.eventRegistre.onerror = error => {
      console.log(error);
    };
  }

  /**
   * Funcion encargada de iniciar el SSE correspondiente al estado de la posicion
   */
  private statusPosition() {
    this.eventRegistreStatus = this.eventService.createEventSource('https://game.everisdigital.com/api/posstatus');
    this.eventRegistreStatus.addEventListener('message', message => {
      const status = JSON.parse(message.data);
      console.log('Position', status);
      if (!status) {
        this.router.navigateByUrl('/finish');
      }
    });
    this.eventRegistreStatus.onerror = error => {
      console.log(error);
    };

  }

  /**
   * Funcion que se encarga de inicar el timer del tiempo de respuesta
   */
  private startTimer() {
    this.timer = setInterval(() => {
      this.countTime++;
    }, 1);
  }

  /**
   * Funcion que se encarga de ejecutar la primera pregunta
   * inicializa el timer para realizar el conteo del tiempo de los usuario se tardan en responder
   */
  private loadQuestion() {
    this.request.makeRequest(HTTP_METHODS.GET, BASE_URL.QUESTION, END_POINTS.QUESTION).then((response: any) => {
      if (response.status === 200) {
        this.newQuestion = response.body as IQuestion;
        this.startTimer();
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  /**
   * Funcion encargada de evaluar la respuesta de una pregunta
   * si la peticion sale bien, se ejecuta nuevamente una nueva pregunta
   * @param question Pregunta que se esta evaluando
   * @param answer Respuesta seleccionada por el usuario
   */
  public validateAnswer(question: IQuestion, answer: IAnswer) {
    const user: IUser = this.global.getUser();
    if (this.timer < 1) {
      return;
    }
    question.time = Math.abs(this.countTime);
    const body = CONVERT_QUESTION_TO_REQUEST_VALIDATE(question, answer, user);
    this.countTime = 0;
    clearInterval(this.timer);
    this.request.makeRequest(HTTP_METHODS.POST, BASE_URL.QUESTION, END_POINTS.QUESTION, body).then((response: any) => {
      if (response.status === 200 || response.status === 202) {
        this.newQuestion = response.body as IQuestion;
        this.loadQuestion();
      }
    }).catch((err) => {
      console.log(err);
    });
  }
}
