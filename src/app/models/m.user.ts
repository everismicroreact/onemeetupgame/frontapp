import { IUser } from '../interfaces/i.user';

/**
 * Funcion que se encarga de crear el objeto para crear un usuario y enviarlo al servidor
 * @param user Usuario local
 */
export function CREATE_REQUEST_USER(user: IUser) {
  const body = {
    player: {
      user: user.name,
      identification: user.indentification
    },
    vehicle: {
      color: user.vehicle.color,
      number: user.vehicle.number.toUpperCase()
    }
  };
  return body;
}
