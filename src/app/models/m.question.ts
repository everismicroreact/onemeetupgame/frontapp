
import { IQuestion, IAnswer } from '../interfaces/i.question';
import { IUser } from '../interfaces/i.user';


/**
 * Funcion que se encarga de crear el objeto final que envia la informacion al servicio cuando se
 * valida la informacion de una pregunta
 */
export function CONVERT_QUESTION_TO_REQUEST_VALIDATE(question: IQuestion, answer: IAnswer, user: IUser) {
  if (!user) {
    user = {
      name: 'Reon',
      indentification: '1ooo0cas',
      vehicle: {
        color: 'red',
        number: 'HTO983'
      },
      mov: 0
    };
  }
  const response = {
    question: {
      id: question.id,
      options: [
        {
          letterOption: answer.letterOption
        }
      ]
    },
    player: {
      identification: user.indentification,
      user: user.name
    },
    miliseconds: question.time
  };
  return response;
}
